import React, { Component } from 'react';
import Counter from './counter.jsx'
class Prod extends Component {
   





    render() { 
        return ( 
            <div>
                <button className="btn btm-secondary m-2" onClick={this.props.Reset}>{'Restart'}
                </button>
                {this.props.Products.map(product=> (
                    <Counter key={product.id} price={product.price} im={product.image} count = {product.count} id ={product.id} product = {product}
                    HIncrement = {this.props.Increment}
                    HDecrement = {this.props.Decrement}
                    HDelete = {this.props.Delete}  />
                ))}
            </div>
         );
    }
}
 
export default Prod;
