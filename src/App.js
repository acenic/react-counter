import React, { Component } from 'react';
import Pro from './components/pr.jsx';
import Nav from './components/nav.jsx';

class App extends Component {

  state = { products:[{id:0,price:10,count:0,image:'./book.jpeg'},
                        {id:1,price:5,count:0,image:'./cd.jpg'},
                        {id:2,price:30,count:0,image:'./flash.jpg'},
                        {id:3,price:50,count:0,image:'./ssd.jpg'},
                        {id:4,price:500,count:0,image:'./phone.png'},
    ], toPay:0 }



  render() {
    return (
      
      <div>
        <Nav toPay = {this.state.toPay} />
        <Pro Increment = {this.clickIncremnt} 
             Decrement = {this.clickDecrement}
             Delete = {this.clickDelete}
             Reset = {this.clickReset}
             Products = {this.state.products}/>
      </div>
      
    );
  }

  clickIncremnt =  product  =>
  {
      const products = [...this.state.products];
      const i=products.indexOf(product);
      if(product.count<10)
      {
      products[i] = { ...product };
      products[i].count++;
      this.toPayPlus(product);
      this.setState({ products });
      }
      
  }
  clickDecrement = product =>
  {
    const products = [...this.state.products];
    const i=products.indexOf(product);
    if(product.count>0)
    {
    products[i] = { ...product };
    products[i].count--;
    this.toPayMinus(product);
    this.setState({ products });
    }
    
  }

  clickReset = () =>
  {
    const products=this.state.products.map(p => {
      p.count=0;
      return p;
    });
    const pay=this.state.toPay=0;
    this.setState({ products  });
    this.setState({ pay });
  };

  clickDelete = product => {
    const products = this.state.products.filter(p => p.id!=product.id);
    if(this.state.toPay>0)
    this.toPayDelete(product);
    this.setState({ products });
  }
  toPayPlus = product =>
  {
    
    
      let toPay = this.state.toPay
      
      toPay += product.price;
      this.setState({ toPay });
    
    
  }
  toPayMinus = product =>
  {
    
    
      let toPay = this.state.toPay
      
      toPay -= product.price;
      this.setState({ toPay });
    
    
  }
  toPayDelete = product =>
  {
    
    
      let toPay = this.state.toPay
      
      toPay -= product.price*product.count;
      this.setState({ toPay });
    
    
  }


}

export default App;
